# Gitlab-CI | Deploy "review" and automate the removal of "review" environments

<p align="center">
Please find the specifications by clicking
  <a href="https://github.com/eazytraining/gitlab-ci-training.git" alt="Crédit : eazytraining.fr" >
  </a>
</p>

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


<img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 

LinkedIn : https://www.linkedin.com/in/carlinfongang/


## Objectif
The objective of this lab is to set up a CI/CD pipeline that integrates the deployment of a review environment triggered by a merge request and the automatic removal of this environment once the merge request is approved by the administration/manager and the new feature is promoted to production.

1. Add a "Deploy review" stage before staging and production to test the functionalities pushed by a new branch.
2. Add a "Stop review" stage to remove the review environment once the validation of the new feature is confirmed, and the merge is completed.

## Successful review environment job
>![Alt text](img/image-9.png)

## Images "review" available on registry
>![Alt text](img/image.png)

>![Alt text](img/image-1.png)

## New environnement add
Operate > Environnement 
>![Alt text](img/image-3.png)

## Test on review environnement 
![Alt text](img/image-4.png)

## Create the feature-animation branch merger request
>![Alt text](image-2.png)

## Validation of merge request, launch the merge in the main branch
### Old app, before launch merge
>![Alt text](img/image-5.png)

### Merge operation
>![Alt text](img/image-6.png)
![Alt text](img/image-12.png)

### Launch of pipeline to deploy on prod
>![Alt text](img/image-7.png)
![Alt text](img/image-13.png)

### New app, in main domain after merge validation
>![Alt text](img/image-11.png)

-----
# Add stage "stop review" to delete de review environment after merge validation
## Launch de merge request, and execute de pipeline "Deploy review and stop review"
>![Alt text](img/image-8.png)
>![Alt text](img/image-14.png)

## Launch et merge with a main branch to go on stage "stop review"
>![Alt text](img/image-15.png)

## Delete of review environnemnt
>![Alt text](img/image-16.png)

### The review containair are dileted
>![Alt text](img/image-17.png)

